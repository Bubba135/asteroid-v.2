﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shipmove : MonoBehaviour {

	public Rigidbody2D rb;
	public float thrust;
	public float turnThrust;
	private float thrustInput;
	private float turnInput;
	public float screenTop;
	public float screenBottom;
	public float screenLeft;
	public float screenright;

	public GameObject bullet;
	public float bulletForce;

	public float deathforce;
    public int lives;

    public Color inColor;
    public Color normalColor;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		//get udates form keyboard.
		thrustInput = Input.GetAxis("Vertical");
		turnInput = Input.GetAxis ("Horizontal");

		//check input from the fire key
		if (Input.GetButtonDown ("Fire1")) {
			GameObject newBullet = Instantiate (bullet, transform.position, transform.rotation);
			newBullet.GetComponent<Rigidbody2D> ().AddRelativeForce (Vector2.up * bulletForce);
			Destroy (newBullet, 8.0f);
		}

		//rotota the ship
		transform.Rotate(Vector3.forward * turnInput * Time.deltaTime * -turnThrust);

		//scren raping

		Vector2 newPos = transform.position;
		if (transform.position.y > screenTop) {
			newPos.y = screenBottom;
		}
		if (transform.position.y < screenBottom) {
			newPos.y = screenTop;
		}
		if (transform.position.x > screenright) {
			newPos.x = screenLeft;
		}
		if (transform.position.x < screenLeft) {
			newPos.x = screenright;
		}
		transform.position = newPos;

	}

	void FixedUpdate()
	{
		rb.AddRelativeForce (Vector2.up * thrustInput);
		//rb.AddTorque (-turnInput);
	}

    void Respawn()
    {
        rb.velocity = Vector2.zero;
        transform.position = Vector2.zero;

        SpriteRenderer sr = GetComponent<SpriteRenderer>();
        sr.enabled = true;
        sr.color = inColor;
        Invoke("InvuInerble", 3f);

    }

    void InvuInerble()
    {
        GetComponent<Collider2D>().enabled = true;
        GetComponent<SpriteRenderer>().color = normalColor;
    }

	void lossLife()
	{
		lives--;
		//Repawn
		GetComponent<SpriteRenderer>().enabled = false;
		GetComponent<Collider2D>().enabled = false;
		Invoke ("Respawn", 3f);
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		Debug.Log (col.relativeVelocity.magnitude);
		if (col.relativeVelocity.magnitude > deathforce) {
			lossLife ();


            if(lives <= 0)
            {
                //game over
                GameOver();
            }
		}
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag ("beam")) {
			lossLife ();
		}
	}

    void GameOver()
    {
        CancelInvoke ();
    }
}
