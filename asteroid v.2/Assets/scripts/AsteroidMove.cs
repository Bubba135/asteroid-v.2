﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMove : MonoBehaviour {

	public float maxThrust;
	public float maxTorque;
	public Rigidbody2D rb;
	public float screenTop;
	public float screenBottom;
	public float screenLeft;
	public float screenright;
    public int asteroidSize; //3 = L 2 = M 1 = S
    public GameObject asteroidMedium;
    public GameObject asteroidSmall;

	// Use this for initialization
	void Start () {
		//add a random thrust and torque
		Vector2 thrust = new Vector2(Random.Range(-maxThrust,maxThrust),Random.Range(-maxThrust,maxThrust));
		float torque = Random.Range (-maxTorque, maxTorque);

		rb.AddForce (thrust);
		rb.AddTorque (torque);


        
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 newPos = transform.position;
		if (transform.position.y > screenTop) {
			newPos.y = screenBottom;
		}
		if (transform.position.y < screenBottom) {
			newPos.y = screenTop;
		}
		if (transform.position.x > screenright) {
			newPos.x = screenLeft;
		}
		if (transform.position.x < screenLeft) {
			newPos.x = screenright;
		}
		transform.position = newPos;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
        //check to see if its a bullet
        if (other.CompareTag("bullet"))
        {
            //destroy the bullet
            Destroy(other.gameObject);
            if(asteroidSize == 3)
            {
                //spawn two medium asteroids
                Instantiate(asteroidMedium, transform.position, transform.rotation);
                Instantiate(asteroidMedium, transform.position, transform.rotation);

            }
            else if(asteroidSize == 2)
            {
                //spawn two small asteroid
                Instantiate(asteroidSmall, transform.position, transform.rotation);
                Instantiate(asteroidSmall, transform.position, transform.rotation);
            }
            else if(asteroidSize == 1)
            {
                //remove the asteroid

            }

            Destroy(gameObject);
        }
		
	}

}
