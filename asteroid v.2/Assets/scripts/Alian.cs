﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alian : MonoBehaviour {

	public Transform tr;
	public Vector3 direction;
	public float speed;
	public float shootingDelay; // it will be in sec.
	public float lastShoot = 0f;
	public float bulletSpeed;

	public Transform player;
	public GameObject bullet;

	// Use this for initialization
	void Start () {
		player = GameObject.FindWithTag ("Player").transform;
		tr = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > lastShoot + shootingDelay) {
			//Shoot
			float andle = Mathf.Atan2(direction.y,direction.x) * Mathf.Rad2Deg - 90f;
			Quaternion q = Quaternion.AngleAxis (andle, Vector3.forward);

			//make a bullet
			GameObject newBullet = Instantiate(bullet, transform.position, q);

			newBullet.GetComponent<Rigidbody2D> ().AddRelativeForce (new Vector2 (0f, bulletSpeed));
			lastShoot = Time.time;
		}
		direction = (player.position - transform.position).normalized;
		tr.position = tr.position + (direction * speed);
	}

}